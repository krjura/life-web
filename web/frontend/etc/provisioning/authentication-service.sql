-- This script can be used to prepare the database for testing frontend


TRUNCATE TABLE application_user CASCADE;
TRUNCATE TABLE application_user_basic_info CASCADE;
TRUNCATE TABLE application_user_role CASCADE;
TRUNCATE TABLE tenant CASCADE;
TRUNCATE TABLE tenant_privilege CASCADE;
TRUNCATE TABLE tenant_role CASCADE;
TRUNCATE TABLE token_revocation CASCADE;

INSERT INTO tenant (id, tenant_name) VALUES ('2d78657e-42ac-4abe-85dc-c962e7541522', 'default');

INSERT INTO tenant_role(id, tenant_id, role_name, role_description, default_role) VALUES (1, '2d78657e-42ac-4abe-85dc-c962e7541522', 'ROLE_USER', 'user', TRUE);
ALTER SEQUENCE tenant_role_id_seq RESTART WITH 2;

INSERT INTO tenant_privilege (id, tenant_role_id, privilege_name, privilege_description) VALUES (1, 1, 'PRIV_USER', 'user');
ALTER SEQUENCE tenant_privilege_id_seq RESTART WITH 2;

-- TRUNCATE application_user CASCADE;
