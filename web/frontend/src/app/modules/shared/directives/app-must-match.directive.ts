import {Directive, forwardRef, Input, OnDestroy} from '@angular/core';
import {FormControl, NG_VALIDATORS, Validator} from '@angular/forms';
import {Subscription} from 'rxjs';

import {GlobalLoggingService} from '../services/global-logging.service';

@Directive({
  selector: '[appMustMatchDirective]',
  providers: [{provide: NG_VALIDATORS, useExisting: forwardRef(() => AppMustMatchDirective), multi: true}]
})

export class AppMustMatchDirective implements Validator, OnDestroy {

  @Input() mustMatchLinkedName: string;

  thisControl: FormControl;
  otherControl: FormControl;
  otherControlSubscription: Subscription;

  // since we have subscribed to other control we also need to unsubscribe
  ngOnDestroy(): void {
    GlobalLoggingService.debug('AppMustMatchDirective(): unsubscribing from other control');

    if (this.otherControlSubscription != null ) {
      this.otherControlSubscription.unsubscribe();
    }
  }

  validate(control: FormControl): {[key: string]: any} {
    if (control.value == null) {
      return null;
    } else if (control.parent == null) {
      return null;
    } else if (this.mustMatchLinkedName == null) {
      return null;
    }

    //  init - needs to be done because of otherControl.subscribe
    if (this.otherControl == null) {
      GlobalLoggingService.debug('AppMustMatchDirective(): other linked control is ' + this.mustMatchLinkedName);
      GlobalLoggingService.debug('AppMustMatchDirective(): other control is null');

      this.otherControl = control.parent.get(this.mustMatchLinkedName) as FormControl;
      this.thisControl = control;

      if (this.otherControl == null) {
        throw new Error('AppMustMatchDirective(): other control is not found');
      }

      GlobalLoggingService.debug('AppMustMatchDirective(): subscribing to other control');
      this.otherControlSubscription = this.otherControl.valueChanges.subscribe(() => {
        this.thisControl.updateValueAndValidity();
      });
    }

    return this.check();
  }

  check(): {[key: string]: any}   {

    if (this.thisControl == null || this.otherControl == null) {
      GlobalLoggingService.debug('AppMustMatchDirective(): Controls are null. Nothing to compare');
      return null;
    }

    if (this.thisControl.value === this.otherControl.value) {
      return null;
    } else {
      return {appMustMatchDirective: true};
    }
  }
}
