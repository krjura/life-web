import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';
import {Directive, forwardRef} from '@angular/core';
import * as moment from 'moment';

import {GlobalLoggingService} from '../services/global-logging.service';

@Directive({
  selector: '[appValidDate]',
  providers: [{provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidDateDirective), multi: true}]
})

export class ValidDateDirective implements Validator {

  validate(control: AbstractControl): {[key: string]: any} {
    const value = control.value;

    if (value == null) {
      return null;
    }

    const trimmedValue = value.trim();

    const isValid = moment(trimmedValue, 'DD.MM.YYYY', true).isValid();
    GlobalLoggingService.debug(trimmedValue + ' is valid check' + isValid);

    if (isValid) {
      return null;
    } else {
      return {'valid': false};
    }
  }
}
