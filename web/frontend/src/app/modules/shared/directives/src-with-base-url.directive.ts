import {Directive, ElementRef, Inject, Input, OnInit} from "@angular/core";

@Directive({
  selector: '[appSrcWithBaseUrl]'
})
export class SrcWithBaseUrlDirective implements OnInit {

  @Input('appSrcWithBaseUrl') url: string;

  constructor(
    @Inject('BASE_URL') private baseUrl: string,
    private el: ElementRef) {

  }

  ngOnInit(){
    this.el.nativeElement.src = this.parseBaseUrl() + this.url;
  }

  public parseBaseUrl():string {
    const parsedUrl: URL = new URL(this.baseUrl);

    let url = null;

    if (parsedUrl.pathname.endsWith('/')) {
      url = parsedUrl.pathname
    } else {
      url = parsedUrl.pathname + '/'
    }

    if (url.startsWith('/')) {
      url = url.substr(1);
    }

    return url;
  }
}
