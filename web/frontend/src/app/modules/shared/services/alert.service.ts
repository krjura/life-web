import {Injectable} from '@angular/core';
import {Toast, ToasterService} from 'angular2-toaster';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class AlertService {

  private static makeToast(type: string, title: string, body: string, timeout: number): Toast {
    return {
      type: type,
      title: title,
      body: body,
      timeout: timeout
    };
  }

  constructor(
    private toasterService: ToasterService,
    private translate: TranslateService) {

  }

  public showSuccessAlertWithTranslate(title: string, message: string, titleArgs: any, messageArgs: any) {

    const translatedTitle = this.translate.instant(title, titleArgs);
    const translatedMessage = this.translate.instant(message, messageArgs);

    this.toasterService.pop( AlertService.makeToast('success', translatedTitle, translatedMessage, 5000));
  }

  public showErrorAlertWithTranslate(title: string, message: string, titleArgs: any, messageArgs: any) {

    const translatedTitle = this.translate.instant(title, titleArgs);
    const translatedMessage = this.translate.instant(message, messageArgs);

    this.toasterService.pop( AlertService.makeToast('error', translatedTitle, translatedMessage, 10000));
  }
}
