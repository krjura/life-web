import {Injectable} from '@angular/core';

import {LoggedInUserInfoResponseModel} from '@shared/model/security/logged-in-user-info-response.model';
import {RegisterUserResponseModel} from '@shared/model/security/register-user-response.model';

@Injectable()
export class GlobalDataService {

  private userInfo: LoggedInUserInfoResponseModel;
  private authenticated: boolean;
  private lastRegisteredUser: RegisterUserResponseModel;

  private grantMap = {};

  constructor() {
    this.authenticated = false;
  }

  public isAuthenticated(): boolean {
    return this.authenticated;
  }

  setAuthenticated(authenticated: boolean) {
    this.authenticated = authenticated;
  }

  public getUserInfo(): LoggedInUserInfoResponseModel {
    return this.userInfo;
  }

  public setUserInfo(userInfo: LoggedInUserInfoResponseModel) {
    this.userInfo = userInfo;

    const grantMap: any = {};
    this.userInfo.grantedAuthorities.grants.forEach(value => {
      grantMap[value] = true;
    });

    this.grantMap = grantMap;
  }

  setLastRegisteredUser(lastRegisteredUser: RegisterUserResponseModel) {
    this.lastRegisteredUser = lastRegisteredUser;
  }

  getLastRegisteredUser() {
    return this.lastRegisteredUser;
  }

  getGrantMap(): any {
    return this.grantMap;
  }

  hasGrantPrivilegeUser(): boolean {
    return this.grantMap.hasOwnProperty('PRIV_USER');
  }

  logout() {
    this.authenticated = false;
    this.userInfo = null;
    this.grantMap = {};
  }
}
