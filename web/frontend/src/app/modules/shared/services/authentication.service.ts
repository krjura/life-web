import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

import {RegisterDataModel} from '@shared/model/security/register-data.model';
import {LoginDataModel} from '@shared/model/security/login-data.model';
import {AuthenticationResponseModel} from '@shared/model/security/authentication-response.model';
import {LoggedInUserInfoResponseModel} from '@shared/model/security/logged-in-user-info-response.model';
import {RegisterUserResponseModel} from '@shared/model/security/register-user-response.model';
import {GlobalLoggingService} from '@shared/services/global-logging.service';
import {AlertService} from "@shared/services//alert.service";
import {GlobalDataService} from '@shared/services//global-data.service';

@Injectable()
export class AuthenticationService {

  private static defaultRequestOptions() {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');

    // we need withCredentials: true because we need to send cookies along with request
    return {
      headers: headers,
      withCredentials: true
    };
  }

  private static defaultNoContentRequestOptions(): any {
    // we need withCredentials: true because we need to send cookies along with request
    return {
      withCredentials: true
    };
  }

  constructor(
    private http: HttpClient,
    private globalDataService: GlobalDataService,
    private alertService: AlertService) {

  }

  loadConfig(): Promise<boolean> {

    return new Promise(( resolve, reject) => {
      this
        .http.get<LoggedInUserInfoResponseModel>('/api/v1/auth/info')
        .subscribe(response => {
          this.globalDataService.setUserInfo(response);
          this.globalDataService.setAuthenticated(true);
          resolve(true);
        }, error => {
          GlobalLoggingService.error('cannot fetch user info. got the following error. ' + JSON.stringify(error, null, 2));
          this.globalDataService.setAuthenticated(true);
          resolve(false);
        });
    });
  }

  registerUser(registerData: RegisterDataModel): Observable<RegisterUserResponseModel> {
    const defaultTenantId = '2d78657e-42ac-4abe-85dc-c962e7541522';

    const request = {
      username: registerData.email,
      password: registerData.password,
      tenantId: defaultTenantId
    };

    const url = '/api/v1/auth/tenant/' + defaultTenantId + '/users/register';

    return this
      .http
      .post<RegisterUserResponseModel>(
        url,
        request,
        AuthenticationService.defaultRequestOptions())
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  login(loginData: LoginDataModel): Observable<AuthenticationResponseModel> {
    const defaultTenantId = '2d78657e-42ac-4abe-85dc-c962e7541522';

    const request = {
      username: loginData.email,
      password: loginData.password,
    };

    const url = '/api/v1/auth/tenant/' + defaultTenantId + '/login';

    return this
      .http
      .post<AuthenticationResponseModel>(
        url,
        request,
        AuthenticationService.defaultRequestOptions())
      .pipe(catchError(err => {
        return throwError(err);
      }));
  }

  logout() {
    const url = '/api/v1/auth/logout';

    return new Observable(observer => {

      this
        .http
        .delete(url, AuthenticationService.defaultNoContentRequestOptions())
        .subscribe(response => {

          this.globalDataService.logout();

          observer.next("logged out");
          observer.complete();

        }, error => {
          this
            .alertService
            .showErrorAlertWithTranslate('security.logout.errorTitle', 'security.logout.errorMessage', {}, {});

          observer.error("error");
          observer.complete();
        });
    })
  }
}
