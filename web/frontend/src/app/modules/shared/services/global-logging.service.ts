export class GlobalLoggingService {

  public static debug(message: string) {
    console.log(message);
  }

  public static info(message: string) {
    console.log(message);
  }

  public static warn(message: string) {
    console.log(message);
  }

  public static error(message: string) {
    console.log(message);
  }

  public static errorException(message: string, error: any) {
    console.log(message, error);
  }
}
