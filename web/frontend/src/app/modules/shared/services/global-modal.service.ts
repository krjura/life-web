import {
  ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, EventEmitter, Injectable,
  Injector
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ConfirmationModalComponent} from '@shared/components/confirmation-modal/confirmation-modal.component';

@Injectable()
export class GlobalModalService {

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    private translateService: TranslateService) {

  }

  openNewModal(bodyTranslationKey: string, bodyParams: any): EventEmitter<string> {
    const resultEmitter: EventEmitter<string> = new EventEmitter();

    // 1. Create a component reference from the component
    const componentRef = this.componentFactoryResolver
      .resolveComponentFactory(ConfirmationModalComponent)
      .create(this.injector);

    componentRef.instance.bodyText = this.translateService.instant(bodyTranslationKey, bodyParams);

    const componentActions: EventEmitter<string> = componentRef.instance.componentActions;

    componentActions.subscribe( message => {
      resultEmitter.emit(message);

      // destroy result emitter
      resultEmitter.complete();
      resultEmitter.unsubscribe();

      // remove component
      this.appRef.detachView(componentRef.hostView);
      componentRef.destroy();
    });

    // 2. Attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(componentRef.hostView);

    // 3. find html element to inject
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    // 4. Append DOM element to the in angular root app
    document.getElementsByTagName('app-root')[0].appendChild(domElem);

    return resultEmitter;
  }
}
