export class RegisterDataModel {
  public email: string;
  public password: string;
  public retypePassword: string;
  public agreedOnTerms: boolean;
}
