export class GrantedAuthoritiesModel {
  roles: string[];
  privileges: string[];
  grants: string[];
}
