export class UserModel {
  id: string;
  tenantId: string;
  username: string;
  enabled: boolean;
}
