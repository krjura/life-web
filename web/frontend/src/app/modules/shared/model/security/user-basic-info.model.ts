export class UserBasicInfoModel {
  firstName: string;
  middleName: string;
  lastName: string;
}
