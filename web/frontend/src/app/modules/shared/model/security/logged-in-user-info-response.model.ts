import {UserBasicInfoModel} from './user-basic-info.model';
import {GrantedAuthoritiesModel} from './granted-authorities.model';

export class LoggedInUserInfoResponseModel {
  username: string;
  basicInfo: UserBasicInfoModel;
  grantedAuthorities: GrantedAuthoritiesModel;
}
