export class AuthenticationResponseModel {
  authorized: boolean;
  providerType: string;
  tenantId: string;
  grants: string[];
}
