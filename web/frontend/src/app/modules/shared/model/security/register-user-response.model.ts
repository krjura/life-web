import {UserModel} from './user.model';
import {UserBasicInfoModel} from './user-basic-info.model';

export class RegisterUserResponseModel {
  public user: UserModel;
  public role: string;
  public basicInfo: UserBasicInfoModel;
}
