import { APP_INITIALIZER, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

import { ApplicationSidebarComponent,  } from '@shared/components/application-sidebar/application-sidebar.component';
import { LocalizationMenuComponent } from '@shared/components/localization-menu/localization-menu.component';
import { ApplicationModalComponent } from '@shared/components/application-modal/application-modal.component';
import { ConfirmationModalComponent } from '@shared/components/confirmation-modal/confirmation-modal.component';
import { ApplicationPaginationComponent } from '@shared/components/application-pagination/application-pagination.component';
import { ApplicationWideSidebarComponent } from '@shared/components/application-wide-sidebar/application-wide-sidebar.component';
import { ApplicationNavMenuComponent } from '@shared/components/application-nav-menu/application-nav-menu.component';
import { AlertService } from '@shared/services/alert.service'
import { GlobalModalService } from '@shared/services/global-modal.service';
import { AuthenticationService } from '@shared/services/authentication.service';
import { PrivilegeUserGuard } from '@shared/guards/priv-user.guard';
import { AppMustMatchDirective } from '@shared/directives/app-must-match.directive';
import { ValidDateDirective } from '@shared/directives/valid-date.directive';
import { SrcWithBaseUrlDirective } from '@shared/directives/src-with-base-url.directive';

export function getBaseUrl() {
  return  document.getElementsByTagName('base')[0].href;
}

const routes: Routes = [

];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),

    TranslateModule.forChild()
  ],
  declarations: [
    ApplicationSidebarComponent,
    LocalizationMenuComponent,
    ApplicationModalComponent,
    ConfirmationModalComponent,
    ApplicationPaginationComponent,
    ApplicationWideSidebarComponent,
    ApplicationNavMenuComponent,

    AppMustMatchDirective,
    ValidDateDirective,
    SrcWithBaseUrlDirective
  ],
  entryComponents: [
    ConfirmationModalComponent
  ],
  providers: [
    PrivilegeUserGuard,
    GlobalModalService,
    AlertService,
    AuthenticationService,
    {
      provide: APP_INITIALIZER,
      useFactory: (authenticationService: AuthenticationService) => () => authenticationService.loadConfig(),
      deps: [AuthenticationService],
      multi: true
    },
    {
      provide: 'BASE_URL', useFactory: getBaseUrl
    }
  ],
  exports : [
    ApplicationSidebarComponent,
    LocalizationMenuComponent,
    ApplicationModalComponent,
    ConfirmationModalComponent,
    ApplicationPaginationComponent,
    ApplicationWideSidebarComponent,
    ApplicationNavMenuComponent,

    AppMustMatchDirective,
    ValidDateDirective,
    SrcWithBaseUrlDirective
  ]
})
export class SharedModule {

}
