import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {AlertService} from '../services/alert.service';
import {GlobalLoggingService} from '../services/global-logging.service';

import {GlobalDataService} from '../services/global-data.service';

@Injectable()
export class PrivilegeUserGuard implements CanActivate {

  constructor(
    private globalDataService: GlobalDataService,
    private alertService: AlertService,
    private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.globalDataService.hasGrantPrivilegeUser()) {
      GlobalLoggingService.info('user has PRIV_USER grant');
      return true;
    }

    this.alertService
      .showErrorAlertWithTranslate('security.guard.privilegeUser.title', 'security.guard.privilegeUser.message', {}, {});

    this.router.navigate(['/security/login']);
    return false;
  }
}
