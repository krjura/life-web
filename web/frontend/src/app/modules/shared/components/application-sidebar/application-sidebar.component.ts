import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ApplicationSidebarElementsModel} from './model/application-sidebar-elements.model';

@Component({
  selector: 'app-application-sidebar',
  templateUrl: './application-sidebar.component.html',
  styleUrls: ['./application-sidebar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationSidebarComponent implements OnInit {

  sidebarActive: boolean = false;

  @Input()
  model: ApplicationSidebarElementsModel;

  constructor() {

  }

  ngOnInit() {

  }

  onSidebarToggle($event) {
    $event.preventDefault();

    this.sidebarActive = ! this.sidebarActive
  }
}
