import {ApplicationSidebarGroupModel} from './application-sidebar-group.model';

export const TYPE_MENU = 'menu';
export const TYPE_MENU_ITEM = 'menu-item';

export class ApplicationSidebarElementsModel {

  constructor(readonly groups: ApplicationSidebarGroupModel[]) {

  }
}
