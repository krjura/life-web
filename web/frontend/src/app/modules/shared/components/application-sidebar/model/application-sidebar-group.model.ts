export class ApplicationSidebarGroupModel {

  constructor(
    readonly name = 'NONE',
    readonly type = 'NONE',
    readonly link?,
    readonly icon?) {

  }
}
