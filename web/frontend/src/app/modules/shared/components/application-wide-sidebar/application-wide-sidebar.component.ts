import {ChangeDetectionStrategy, Component} from '@angular/core';

import {
  ApplicationSidebarElementsModel,
  TYPE_MENU, TYPE_MENU_ITEM
} from '../application-sidebar/model/application-sidebar-elements.model';
import { ApplicationSidebarGroupModel } from '../application-sidebar/model/application-sidebar-group.model';

@Component({
  selector: 'app-application-wide-sidebar',
  templateUrl: './application-wide-sidebar.component.html',
  styleUrls: ['./application-wide-sidebar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationWideSidebarComponent {

  sidebar: ApplicationSidebarElementsModel;

  constructor() {
    this.setupSidebar();
  }

  setupSidebar() {
    this.sidebar = new ApplicationSidebarElementsModel(
      [
        new ApplicationSidebarGroupModel(
          'shared.sidebar.main',
          TYPE_MENU),
        new ApplicationSidebarGroupModel(
          'shared.sidebar.users.home',
          TYPE_MENU_ITEM,
          '/users/home',
          'icon mdi mdi-home'),
        new ApplicationSidebarGroupModel(
          'shared.sidebar.birthdays.home',
          TYPE_MENU_ITEM,
          '/birthdays/home',
          'icon mdi mdi-folder-person')
      ]
    );
  }
}
