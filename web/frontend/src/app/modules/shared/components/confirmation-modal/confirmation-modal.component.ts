import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnDestroy {

  @Output()
  componentActions: EventEmitter <string> = new EventEmitter();

  bodyText;

  constructor() { }

  ngOnDestroy() {
    this.componentActions.complete();
    this.componentActions.unsubscribe();
  }

  userConfirmed() {
    this.componentActions.emit('confirmed');
  }

  userRejected() {
    this.componentActions.emit('rejected');
  }
}
