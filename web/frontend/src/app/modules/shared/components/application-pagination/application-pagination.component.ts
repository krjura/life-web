import {ChangeDetectionStrategy, Component, EventEmitter, OnDestroy, Output} from '@angular/core';

@Component({
  selector: 'app-application-pagination',
  templateUrl: './application-pagination.component.html',
  styleUrls: ['./application-pagination.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationPaginationComponent implements OnDestroy {

  @Output()
  componentActions: EventEmitter <number> = new EventEmitter();

  currentPageNumber = 1;

  paginationInfo = {
    minusFive: {
      enabled: false,
      show: true
    },
    previous: {
      enabled: false,
      show: true
    },
    next: {
      enabled: true,
      show: true
    },
    plusFive: {
      enabled: true,
      show: true
    },
  };

  constructor() {
    this.update();
  }

  ngOnDestroy() {
    this.componentActions.complete();
    this.componentActions.unsubscribe();
  }

  private updatePageNumber(pageNumber: number) {

    if ( pageNumber < 0 ) {
      this.currentPageNumber = 0;
    } else {
      this.currentPageNumber = pageNumber;
    }

    this.update();

    this.componentActions.emit(this.currentPageNumber);
  }

  private update() {
    this.paginationInfo.minusFive.enabled = this.currentPageNumber > 5;
    this.paginationInfo.previous.enabled = this.currentPageNumber > 1;
  }

  minusFiveClicked() {
    this.updatePageNumber(this.currentPageNumber - 5);
  }

  previousClicked() {
    this.updatePageNumber(this.currentPageNumber - 1);
  }

  nextClicked() {
    this.updatePageNumber(this.currentPageNumber + 1);
  }

  plusFiveClicked() {
    this.updatePageNumber(this.currentPageNumber + 5);
  }
}
