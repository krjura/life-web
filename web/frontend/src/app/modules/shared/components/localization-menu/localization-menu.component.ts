import { Component, OnInit } from '@angular/core';
import { GlobalLoggingService } from '../../services/global-logging.service';

@Component({
  selector: 'app-localization-menu',
  templateUrl: './localization-menu.component.html',
  styleUrls: ['./localization-menu.component.css']
})
export class LocalizationMenuComponent implements OnInit {

  showLanguage: boolean;

  constructor() {

  }

  ngOnInit() {
  }

  toggleLanguage() {
    this.showLanguage = !this.showLanguage;
    GlobalLoggingService.debug('show language flag is now ' + this.showLanguage);
  }

}
