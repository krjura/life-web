import {Component, ComponentFactoryResolver, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-application-modal',
  templateUrl: './application-modal.component.html',
  styleUrls: ['./application-modal.component.css']
})
export class ApplicationModalComponent {

  @Output() modalActions: EventEmitter <string> = new EventEmitter();

  @Input() title = 'Title';

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {

  }

  onClose() {
    this.modalActions.emit('close');
  }
}
