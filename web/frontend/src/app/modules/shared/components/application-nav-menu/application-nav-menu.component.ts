import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {GlobalDataService} from "@shared/services/global-data.service";
import {AuthenticationService} from "@shared/services/authentication.service";

@Component({
  selector: 'app-application-nav-menu',
  templateUrl: './application-nav-menu.component.html',
  styleUrls: ['./application-nav-menu.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApplicationNavMenuComponent implements OnInit {

  active: boolean = false;
  username: string;

  constructor(
    private globalDataService:GlobalDataService,
    private authenticationService:AuthenticationService,
    private router: Router) {

    this.username = globalDataService.getUserInfo().username;
  }

  ngOnInit() {

  }

  onSelect($event) {
    $event.preventDefault();

    this.active = ! this.active;
  }

  onLogout($event) {
    $event.preventDefault();

    this
      .authenticationService
      .logout()
      .subscribe(result => {
        this.router.navigate(['/security/login']);
      });
  }
}
