import { Component } from '@angular/core';
import {
  ApplicationSidebarElementsModel, TYPE_MENU, TYPE_MENU_ITEM
} from '@shared/components/application-sidebar/model/application-sidebar-elements.model';
import {
  ApplicationSidebarGroupModel
} from '@shared/components/application-sidebar/model/application-sidebar-group.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  sidebar: ApplicationSidebarElementsModel;

  constructor() {
    this.setupSidebar();
  }

  setupSidebar() {
    this.sidebar = new ApplicationSidebarElementsModel(
      [
        new ApplicationSidebarGroupModel( 'users.home.sidebar.applications', TYPE_MENU ),
        new ApplicationSidebarGroupModel( 'users.home.sidebar.birthdays', TYPE_MENU_ITEM, '/birthdays/home')
      ]
    );
  }
}
