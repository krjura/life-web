import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { SharedModule } from '@shared/shared.module';
import { PrivilegeUserGuard } from "@shared/guards/priv-user.guard";

import { HomeComponent } from '@users/pages/home/home.component';
import { UsersComponent } from '@users/users.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [PrivilegeUserGuard],
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [PrivilegeUserGuard],
  }
];

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild(routes),

    SharedModule
  ],
  declarations: [
    HomeComponent,
    UsersComponent
  ]
})
export class UsersModule {

}
