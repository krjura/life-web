import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

import {SharedModule} from '@shared/shared.module';

import {LoginComponent} from '@security/pages/login/login.component';
import {RegisterComponent} from '@security/pages/register/register.component';
import {SecurityComponent} from '@security/security.component';

const routes: Routes = [
  {
    path: '',
    component: SecurityComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),

    TranslateModule,

    SharedModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    SecurityComponent,
  ],
  providers: [

  ],
  exports: [

  ]
})
export class SecurityModule {

}
