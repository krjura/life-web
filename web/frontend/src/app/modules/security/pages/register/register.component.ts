import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

import {RegisterDataModel} from '@shared/model/security/register-data.model';
import {AlertService} from '@shared/services/alert.service';
import {GlobalLoggingService} from '@shared/services/global-logging.service';
import {GlobalDataService} from '@shared/services/global-data.service';
import {AuthenticationService} from '@shared/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerData: RegisterDataModel;

  constructor(
    private authenticationService: AuthenticationService,
    private globalDataService: GlobalDataService,
    private router: Router,
    private alertService: AlertService) {

  }

  ngOnInit() {
    this.registerData = new RegisterDataModel();
  }

  submit(form: NgForm) {
    this
      .authenticationService
      .registerUser(this.registerData)
      .subscribe(response => {
        GlobalLoggingService.info('registered new user');

        this.globalDataService.setLastRegisteredUser(response);

        this.alertService.showSuccessAlertWithTranslate(
            'security.register.alert.success.title',
            'security.register.alert.success.message',
            {}, {email: this.registerData.email});

        const localRouter = this.router;
        setTimeout(function () {
          localRouter.navigate(['/login']);
        }, 1000);
      });
  }
}
