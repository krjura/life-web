import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

import {AlertService} from '@shared/services/alert.service';
import {AuthenticationService} from '@shared//services/authentication.service';
import {LoginDataModel} from '@shared/model/security/login-data.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: LoginDataModel;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private alertService: AlertService) {

    this.loginData = new LoginDataModel();
  }

  ngOnInit() {

  }

  submit(form: NgForm) {
    this
      .authenticationService
      .login(this.loginData)
      .subscribe(response => {
        if (response.authorized) {
          this.loadUserInfo();
        } else {
          this.alertService.showErrorAlertWithTranslate(
            'security.login.alert.error.title',
            'security.login.alert.error.message',
            {}, {});
        }
      });
  }

  private loadUserInfo() {
    this.authenticationService.loadConfig().then(response => {

      if (response ) {
        this.alertService.showSuccessAlertWithTranslate(
          'security.login.alert.success.title',
          'security.login.alert.success.message',
          {}, {});

        const localRouter = this.router;
        setTimeout(function () {
          localRouter.navigate(['/users/home']);
        }, 1000);

      } else {
        this.alertService.showErrorAlertWithTranslate(
          'security.login.alert.errorForInfo.title',
          'security.login.alert.errorForInfo.message',
          {}, {});
      }
    });
  }
}
