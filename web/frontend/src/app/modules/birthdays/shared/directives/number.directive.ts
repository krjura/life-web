import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

import { GlobalLoggingService } from '@shared/services/global-logging.service';

@Directive({
  selector: '[appMustBeNumber]',
  providers: [{provide: NG_VALIDATORS, useExisting: NumberDirective, multi: true}]
})
export class NumberDirective implements Validator {
  @Input() max: number;
  @Input() min: number;

  validate(c: FormControl): {[key: string]: any} {
    const v = c.value;

    if ( !this.max || !this.min) {
      GlobalLoggingService.info('max or min not defined. Max is ' + this.max + '; min is ' + this.min);
    }

    if (v < this.min) {
      GlobalLoggingService.info('value is smaller then allowed ' + this.min);
      return {'min': true};
    } else if ( v > this.max) {
      GlobalLoggingService.info('value is larger then allowed ' + this.max);
      return {'max': true};
    } else {
      return null;
    }
  }
}
