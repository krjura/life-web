export class PagingResponseModel {
  public maxPerPage: number;
  public pageNumber: number;
  public numberOfResults: number;
}
