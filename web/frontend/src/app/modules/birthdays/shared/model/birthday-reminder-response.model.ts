export class BirthdayReminderResponseModel {
  public id: number;
  public birthdayId: number;
  public type: string;
  public units: number;
  public remindAt: string;
  public repeatable: boolean;
}
