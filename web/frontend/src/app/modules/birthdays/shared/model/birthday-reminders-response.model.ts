import {BirthdayReminderResponseModel} from './birthday-reminder-response.model';
import {PagingResponseModel} from './paging-response.model';

export class BirthdayRemindersResponseModel {

  public content: BirthdayReminderResponseModel[];

  public paging: PagingResponseModel;

}
