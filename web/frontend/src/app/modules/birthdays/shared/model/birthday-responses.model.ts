import {BirthdayResponseModel} from './birthday-response.model';

export class BirthdayResponsesModel {

  public content: BirthdayResponseModel[];
}
