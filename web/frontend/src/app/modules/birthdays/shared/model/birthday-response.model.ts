export class BirthdayResponseModel {

  public id: number;
  public userId: string;
  public bornOn: number;
  public nextBirthday: number;
  public personInfo: string;
  public note: string;

}
