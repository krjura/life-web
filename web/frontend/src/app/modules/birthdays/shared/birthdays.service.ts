import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as moment from 'moment';

import {AlertService} from '@shared/services/alert.service';

import {BirthdayResponsesModel} from '@birthdays/shared/model/birthday-responses.model';
import {BirthdayResponseModel} from '@birthdays/shared/model/birthday-response.model';
import {BirthdayRemindersResponseModel} from '@birthdays/shared/model/birthday-reminders-response.model';

@Injectable()
export class BirthdaysService {

  public static formatBirthdayDate(birthday: BirthdayResponseModel): string {
    return moment(birthday.bornOn).format('DD.MM.YYYY');
  }

  public static numberToString(value: number) {
    return value >= 10 ? value.toString() : '0' + value;
  }

  private static defaultRequestOptions() {
    const headers = new HttpHeaders();
    headers.append('Content-type', 'application/json');

    // we need withCredentials: true because we need to send cookies along with request
    return {
      headers: headers,
      withCredentials: true
    };
  }

  private static defaultNoContentRequestOptions(): any {
    // we need withCredentials: true because we need to send cookies along with request
    return {
      withCredentials: true
    };
  }

  constructor(private http: HttpClient, private alertService: AlertService) {

  }

  addNewBirthday(date: string, personInfo: string, note: string) {
    const request = {
      birthday: date.trim(),
      personInfo: personInfo.trim(),
      note: note.trim()
    };

    const url = '/api/v1/birthdays';

    return this
      .http
      .post(
        url,
        request,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {
        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  listBirthdays(searchTerm: string, pageNumber: number): Observable<BirthdayResponsesModel> {

    let url = '/api/v1/birthdays?page=' + pageNumber + '&maxPerPage=10';

    if ( searchTerm.length > 0) {
      url = url + '&searchTerm=' + searchTerm;
    }

    return this
      .http
      .get<BirthdayResponsesModel>(
        url,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  deleteBirthday(id: number) {
    const url = '/api/v1/birthdays/' + id;

    return this
      .http
      .delete(
        url,
        BirthdaysService.defaultNoContentRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  editBirthday(id: number, date: string, personInfo: string, note: string) {
    const request = {
      birthday: date.trim(),
      personInfo: personInfo.trim(),
      note: note.trim()
    };

    const url = '/api/v1/birthdays/' + id;

    return this
      .http
      .put(
        url,
        request,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  loadBirthday(id: string): Observable<BirthdayResponseModel> {

    const url = '/api/v1/birthdays/' + id;

    return this
      .http
      .get<BirthdayResponseModel>(
        url,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  loadReminders(id: number): Observable<BirthdayRemindersResponseModel> {
    const url = '/api/v1/birthdays/' + id + '/reminders';

    return this
      .http
      .get<BirthdayRemindersResponseModel>(
        url,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  addNewReminder(
    birthdayId: number,
    units: number,
    type: string,
    remindAt: string,
    repeatable: boolean) {

    const request = {
      type: type.trim(),
      units: units,
      remindAt: remindAt.trim(),
      repeatable: repeatable
    };

    const url = '/api/v1/birthdays/' + birthdayId + '/reminders';

    return this
      .http
      .post(
        url,
        request,
        BirthdaysService.defaultRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }

  deleteReminder(birthdayId: number, reminderId: number) {
    const url = '/api/v1/birthdays/' + birthdayId + '/reminders/' + reminderId;

    return this
      .http
      .delete(
        url,
        BirthdaysService.defaultNoContentRequestOptions())
      .pipe(catchError(err => {

        this.alertService.showErrorAlertWithTranslate(
          'birthdays.services.birthdays.alerts.errors.serverError.title',
          'birthdays.services.birthdays.alerts.errors.serverError.message',
          {}, {});

        return throwError(err);
      }));
  }
}
