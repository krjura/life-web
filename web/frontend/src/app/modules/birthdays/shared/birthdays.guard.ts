import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

import {GlobalLoggingService} from '@shared/services/global-logging.service';
import {AlertService} from '@shared/services/alert.service';
import {GlobalDataService} from '@shared/services/global-data.service';

@Injectable()
export class BirthdaysGuard implements CanActivate {

  constructor(
    private globalDataService: GlobalDataService,
    private alertService: AlertService,
    private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.globalDataService.hasGrantPrivilegeUser()) {
      GlobalLoggingService.info('user has PRIV_USER grant');
      return true;
    }

    this.alertService
      .showErrorAlertWithTranslate('birthdays.guards.privilegeUser.title', 'birthdays.guards.privilegeUser.title', {}, {});

    this.router.navigate(['/login']);

    return false;
  }

}
