import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes} from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@birthdays/pages/home/home.component';
import { BirthdaysComponent } from '@birthdays/birthdays.component';
import { BirthdaysService } from '@birthdays/shared/birthdays.service';
import { BirthdaysGuard } from '@birthdays/shared/birthdays.guard';
import { ChangesComponent } from '@birthdays/pages/changes/changes.component';
import { NumberDirective } from '@birthdays/shared/directives/number.directive';

const routes: Routes = [
  {
    path: '',
    component: BirthdaysComponent,
    canActivate: [BirthdaysGuard],
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'changes',
        component: ChangesComponent
      },
      {
        path: 'changes/:id',
        component: ChangesComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes),

    TranslateModule,

    SharedModule
  ],
  declarations: [
    HomeComponent,
    NumberDirective,
    BirthdaysComponent,
    ChangesComponent,
  ],
  providers: [
    BirthdaysService,
    BirthdaysGuard
  ]
})
export class BirthdaysModule {

}
