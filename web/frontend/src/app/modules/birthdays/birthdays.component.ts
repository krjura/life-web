import { Component } from '@angular/core';
import {
  ApplicationSidebarElementsModel, TYPE_MENU, TYPE_MENU_ITEM
} from '@shared/components/application-sidebar/model/application-sidebar-elements.model';
import { ApplicationSidebarGroupModel } from '@shared/components/application-sidebar/model/application-sidebar-group.model';

@Component({
  selector: 'app-birthdays',
  templateUrl: './birthdays.component.html',
  styleUrls: ['./birthdays.component.css']
})
export class BirthdaysComponent {

  sidebar: ApplicationSidebarElementsModel;

  constructor() {
    this.setupSidebar();
  }

  setupSidebar() {
    this.sidebar = new ApplicationSidebarElementsModel(
      [
        new ApplicationSidebarGroupModel( 'birthdays.home.sidebar.main', TYPE_MENU ),
        new ApplicationSidebarGroupModel( 'birthdays.home.sidebar.temp', TYPE_MENU_ITEM, '/birthdays')
      ]
    );
  }
}
