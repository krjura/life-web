import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import * as moment from 'moment';

import { AlertService} from '@shared/services/alert.service';

import { BirthdayRemindersResponseModel} from '@birthdays/shared/model/birthday-reminders-response.model';
import { BirthdayReminderResponseModel } from '@birthdays/shared/model/birthday-reminder-response.model';
import { BirthdaysService } from '@birthdays/shared/birthdays.service';

export const MODE_ADD = 'ADD';
export const MODE_EDIT = 'EDIT';

@Component({
  selector: 'app-birthdays-changes',
  templateUrl: './changes.component.html',
  styleUrls: ['./changes.component.css']
})
export class ChangesComponent implements OnInit {

  private reminders: BirthdayRemindersResponseModel;

  model = {
    id: null,
    date: '',
    personInfo: '',
    note: ''
  };

  reminderModel = {
    units: 1,
    type: 'DAYS',
    hours: 0,
    minutes: 0
  };

  mode = MODE_ADD;
  initialized = false;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private translate: TranslateService,
    private alertService: AlertService,
    private birthdaysService: BirthdaysService) {

    this.reminders = null;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];

      if (id == null) {
        this.mode = MODE_ADD;
        this.initialized = true;
      } else {
        this.loadBirthdayInfo(id);
        this.loadReminders(id);
      }
    });
  }

  loadBirthdayInfo(id: string) {
    this
      .birthdaysService
      .loadBirthday(id)
      .subscribe( birthday => {
        if (birthday != null) {
          this.model.id = birthday.id;
          this.model.date = BirthdaysService.formatBirthdayDate(birthday);
          this.model.personInfo = birthday.personInfo;
          this.model.note = birthday.note;
          this.mode = MODE_EDIT;

          this.initialized = true;
        }
      });
  }

  loadReminders(id: number) {
    this
      .birthdaysService
      .loadReminders(id)
      .subscribe( reminders => {
        this.reminders = reminders;
      });
  }

  goBackToPreviousPage() {
    this.location.back();
  }

  saveBirthday() {

    const momentDate = moment(this.model.date, 'DD.MM.YYYY', true);
    const serverFormattedDate = momentDate.format('YYYY-MM-DD');

    if (this.mode === MODE_ADD) {
      this
        .birthdaysService
        .addNewBirthday(serverFormattedDate, this.model.personInfo, this.model.note)
        .subscribe(response => {

          this.birthdaySavedSuccessfully(this.model.personInfo);

          this.goBackToPreviousPage();
        });
    } else {
      this
        .birthdaysService
        .editBirthday(this.model.id, serverFormattedDate, this.model.personInfo, this.model.note)
        .subscribe(response => {
          this.birthdaySavedSuccessfully(this.model.personInfo);

          this.goBackToPreviousPage();
        });
    }
  }

  private birthdaySavedSuccessfully(personInfo: string) {
    this.alertService.showSuccessAlertWithTranslate(
      'birthdays.changes.alerts.birthdays.success.saved.title',
      'birthdays.changes.alerts.birthdays.success.saved.message',
      {}, {personInfo: personInfo});
  }

  addReminder() {
    // cannot add model to non existing birthday
    if (this.model.id == null) {
      return;
    }

    // fix hours and minute - must be two digit string
    let hours = '00';
    let minutes = '00';

    if (this.reminderModel.hours < 10) {
      hours = '0' + this.reminderModel.hours;
    }

    if (this.reminderModel.minutes < 10) {
      minutes = '0' + this.reminderModel.minutes;
    }

    // hours, minutes, seconds
    const remindAt: string = hours + ':' + minutes + ':00';

    const birthdayId = this.model.id;

    this
      .birthdaysService
      .addNewReminder(birthdayId, this.reminderModel.units, this.reminderModel.type, remindAt, true)
      .subscribe(response => {
        this.reminderSavedSuccessfully();
        this.loadReminders(birthdayId);
      });
  }

  private reminderSavedSuccessfully() {
    this.alertService.showSuccessAlertWithTranslate(
      'birthdays.changes.alerts.reminders.success.saved.title',
      'birthdays.changes.alerts.reminders.success.saved.message',
      {}, {});
  }

  generateReminderPrettyText(reminder: BirthdayReminderResponseModel): string {
    let typeText = '';

    if (reminder.units === 1) {
      typeText = this.translate.instant('birthdays.changes.reminders.type.' + reminder.type + '_ONE');
    } else {
      typeText = this.translate.instant('birthdays.changes.reminders.type.' + reminder.type + '_MORE');
    }

    const param = {
      units: reminder.units,
      type: typeText,
      remindAt: reminder.remindAt
    };

    return this.translate.instant('birthdays.changes.reminders.reminderText', param);
  }

  deleteReminder(reminder: BirthdayReminderResponseModel, $event: any) {
    $event.preventDefault();

    this
      .birthdaysService
      .deleteReminder(reminder.birthdayId, reminder.id)
      .subscribe(response => {
        this.alertService.showSuccessAlertWithTranslate(
          'birthdays.changes.alerts.reminders.success.deleted.title',
          'birthdays.changes.alerts.reminders.success.deleted.message',
          {}, {});
        this.loadReminders(reminder.birthdayId);
      });
  }
}
