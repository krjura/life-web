import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


import {AlertService} from '@shared/services/alert.service';
import {GlobalModalService} from '@shared/services/global-modal.service';

import {BirthdayResponseModel} from '@birthdays/shared/model/birthday-response.model';
import {BirthdaysService} from '@birthdays/shared/birthdays.service';

@Component({
  selector: 'app-birthdays-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  searchTermEvents = new Subject<string>();
  currentSearchTerm = '';

  birthdaysData: BirthdayResponseModel[];
  birthdaysDataInitialized = false;
  birthdaysDataPageNumber = 1;

  selectedBirthday: BirthdayResponseModel = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private birthdaysService: BirthdaysService,
    private alertService: AlertService,
    private globalModalService: GlobalModalService) {
  }

  ngOnInit() {
    // search term can be specified using a query param
    // so first wait until this is resolved
    // by the way. Why do I need subscribe here for got sake
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      const searchTerm = params['searchTerm'];

      if (searchTerm != null) {
        this.currentSearchTerm = searchTerm;
      }

      // load current birthdays
      this.loadBirthdayData();
      // initialize search subject stream
      this.prepareSearchCheck();
    });
  }

  // update url when search term changes
  updateUrl() {
    // if you know how to do this better let me know
    const currentPath = '/birthdays/home';

    if (this.currentSearchTerm === '') {
      this.router.navigate([currentPath]);
    } else {
      this.router.navigate([currentPath], {queryParams: {searchTerm: this.currentSearchTerm}});
    }
  }

  ngOnDestroy(): void {
    this.searchTermEvents.complete();
    this.searchTermEvents.unsubscribe();
  }

  private loadBirthdayData() {
    this
      .birthdaysService
      .listBirthdays(this.currentSearchTerm, this.birthdaysDataPageNumber)
      .subscribe( result => {
        this.birthdaysData = result.content;
        this.birthdaysDataInitialized  = true;
    });
  }

  // start birthday data table
  formatBirthdayDate(birthday: BirthdayResponseModel) {
    return BirthdaysService.formatBirthdayDate(birthday);
  }

  deleteBirthday(birthday: BirthdayResponseModel, $event: any) {
    $event.preventDefault();

    const resultEmitter: EventEmitter<string> = this
      .globalModalService
      .openNewModal('birthdays.home.alert.deleteBirthdayConfirmation.message', {personInfo: birthday.personInfo})
      .subscribe( result => {
        if ( result === 'confirmed') {
          this.deleteBirthdayAfterConfirmation(birthday);
        }
    });
  }

  deleteBirthdayAfterConfirmation(birthday: BirthdayResponseModel) {
    this
      .birthdaysService
      .deleteBirthday(birthday.id)
      .subscribe( response => {

        this.alertService.showSuccessAlertWithTranslate(
          'birthdays.home.alert.deleteBirthdaySuccess.title',
          'birthdays.home.alert.deleteBirthdaySuccess.message',
          {}, { personInfo: birthday.personInfo});

        this.loadBirthdayData();
    });
  }

  onPageChange(pageNumber: number) {
    this.birthdaysDataPageNumber = pageNumber;
    this.loadBirthdayData();
  }
  // end birthday data table

  // start search bar
  onSearchKeyUp(event) {
    this.searchTermEvents.next(event.target.value);
  }

  private prepareSearchCheck() {
    this
      .searchTermEvents
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(result => {
        this.currentSearchTerm = result;

        if (this.birthdaysDataInitialized) {
          this.loadBirthdayData();
          this.updateUrl();
        }
      });
  }
  // end search bar
}
