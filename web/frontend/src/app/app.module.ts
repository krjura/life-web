import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { TranslateModule } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './core/components/dashboard.component';
import { GlobalDataService } from '@shared/services/global-data.service';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,

    ToasterModule.forRoot(),
    TranslateModule.forRoot(),

    SharedModule
  ],
  providers: [
    GlobalDataService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
