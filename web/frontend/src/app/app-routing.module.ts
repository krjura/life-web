import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrivilegeUserGuard } from "@shared/guards/priv-user.guard";

import { DashboardComponent } from "./core/components/dashboard.component";

const routes: Routes = [
  {
    path: 'security',
    loadChildren: './modules/security/security.module#SecurityModule'
  },
  {
    path: 'users',
    loadChildren: './modules/users/users.module#UsersModule'
  },
  {
    path: 'birthdays',
    loadChildren: './modules/birthdays/birthdays.module#BirthdaysModule'
  },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [PrivilegeUserGuard]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '/dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
