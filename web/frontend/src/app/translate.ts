export const translateEn = {
  security: {
    login: {
      alert: {
        error: {
          title: 'Failure',
          message: 'Invalid username and password'
        },
        errorForInfo: {
          title: 'Failure',
          message: 'Failed to load user information'
        },
        success: {
          title: 'Success',
          message: 'You have been successfully logged In'
        }
      },
      form: {
        title: 'SIGN IN TO CONTINUE.',
        label: {
          forgotPassword: 'Forgot your password?',
          login: 'Login',
          needToSignUp: 'Need to Signup?',
          register: 'Register Now'
        },
        placeholder: {
          email: 'Enter email',
          password: 'Password'
        },
        validation: {
          email: 'Must be a valid email address.',
          password: 'Letters, numbers and #,$ allowed (10-40 chars).'
        }
      }
    },
    logout: {
      menuLogout: "Logout",
      errorTitle: "Server Error",
      errorMessage: "Unable to logout due to server error"
    },
    register: {
      alert: {
        success: {
          title: 'Success',
          message: 'User {{ email }} has been registered'
        }
      },
      form: {
        title: 'SIGNUP TO GET INSTANT ACCESS.',
        label: {
          email: 'Email address',
          password: 'Password',
          retypePassword: 'Retype Password',
          agreedOn: 'I agree with the',
          agreedOnTerms: 'terms',
          createAccount: 'Create account',
          haveAnAccount: 'Have an account?',
          signUp: 'Sign Up'
        },
        placeholder: {
          email: 'Enter email',
          password: 'Password',
          retypePassword: 'Retype Password'
        },
        validation: {
          email: 'Must be a valid email address.',
          password: 'Letters, numbers and #,$ allowed (10-40 chars).',
          retypePassword: 'Letters, numbers and #,$ allowed (10-40 chars).',
          retypePasswordMustMatch: 'Passwords must match.'
        }
      }
    },
    guard: {
      privilegeUser: {
        title: 'Security violation',
        message: 'You do not have PRIV_USER grant. Please contact the administrator if you should.'
      }
    }
  },
  users: {
    home: {
      sidebar: {
        applications: 'Applications',
        birthdays: 'Birthdays'
      }
    }
  },
  shared: {
    localizationMenu: {
      english: 'English',
      croatian: 'Croatian'
    },
    applicationModal: {
      title: 'Sure about this?',
      yesButton: 'Yes',
      noButton: 'No'
    },
    sidebar: {
      main: 'Navigation',
      birthdays: {
        home: 'Birthdays'
      },
      users: {
        home: 'Users'
      }
    }
  },
  birthdays: {
    home: {
      sidebar: {
        main: 'Navigation',
        temp: 'Temporary'
      },
      content: {
        search: {
          welcome: 'Never forget a birthday again',
          title: 'Search',
          buttonAdd: 'Add New',
          placeholderSearchExisting: 'Search'
        },
        data: {
          table: {
            panelHeader: 'Important dates',
            description: 'Description',
            when: 'When',
            actions: 'Changes',
          }
        }
      },
      alert: {
        deleteBirthdayConfirmation: {
          message: 'Are you sure you want to forget about "{{ personInfo }}"?'
        },
        deleteBirthdaySuccess: {
          title: 'Success',
          message: 'You have forgot about "{{ personInfo }}"!'
        },
        noData: 'No birthdays can be found on this page.'
      }
    },
    changes: {
      navigation: {
        goBack: 'Return to the previous page'
      },
      validations: {
        date: 'Must be in MM.DD.YYY format (e.g 05.09.1981)',
        personInfo: 'Person info can be up to 100 characters long',
        note: 'Note can be up to 200 characters long'
      },
      birthdays: {
        titleAdd: 'Add New Birthday',
        titleEdit: 'Edit Birthday',
        dateOfBirth: 'Date of birth (format MM.DD.YYY e.g. 05.09.1981)',
        personInfo: 'Person Info (up to 100 characters)',
        note: 'Note (up to 200 characters)',
        addButton: 'Add new birthday',
        editButton: 'Update birthday',
      },
      reminders: {
        reminderPanelTitle: 'Reminders',
        reminderText: '{{ units}} {{ type }} before at {{ remindAt }}',
        form: {
          type: {
            days: 'days',
            weeks: 'weeks',
            months: 'months'
          },
          beforeAt: 'before at',
          buttonAddReminder: 'Add Reminder'
        },
        table: {
          when: 'When'
        },
        type: {
          DAYS_ONE: 'day',
          DAYS_MORE: 'days',
          WEEKS_ONE: 'week',
          WEEKS_MORE: 'weeks',
          MONTHS_ONE: 'month',
          MONTHS_MORE: 'months'
        },
        noRemindersDefined: ' No reminders are currently defined'
      },
      alerts: {
        birthdays: {
          success: {
            saved: {
              title: 'Success',
              message: 'Birthday for "{{ personInfo}}" was saved successfully'
            }
          }
        },
        reminders: {
          success: {
            saved: {
              title: 'Success',
              message: 'Reminder was saved successfully'
            },
            deleted: {
              title: 'Success',
              message: 'Reminder was deleted successfully'
            }
          }
        }
      }
    },
    services: {
      birthdays: {
        alerts: {
          errors: {
            serverError: {
              title: 'Server Error',
              message: 'Server was unable to complete your request'
            }
          }
        }
      }
    },
    guards: {
      privilegeUser: {
        title: 'Security violation',
        message: 'You do not have PRIV_USER grant. Please contact the administrator if you should.'
      }
    },
    alert: {
      noData: 'There is nothing you can forget on this page'
    }
  },
};
