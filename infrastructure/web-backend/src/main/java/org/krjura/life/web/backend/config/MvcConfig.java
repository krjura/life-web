package org.krjura.life.web.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/index.html")
                .addResourceLocations("file:resources/")
                .setCachePeriod(0);

        registry
                .addResourceHandler("/**")
                .addResourceLocations("file:resources/")
                .setCacheControl(CacheControl
                        .maxAge(1, TimeUnit.DAYS));

    }
}
