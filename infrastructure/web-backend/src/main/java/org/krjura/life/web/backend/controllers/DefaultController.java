package org.krjura.life.web.backend.controllers;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class DefaultController {

    @RequestMapping(
            path = "/",
            method = RequestMethod.GET
    )
    public ResponseEntity<FileSystemResource> index() {

        if(Files.exists(Paths.get("resources/index.html"))) {
            return ResponseEntity.ok(new FileSystemResource("resources/index.html"));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}