package org.krjura.life.web.backend.config;

import org.krjura.life.web.backend.config.pojo.UserInfo;
import org.krjura.life.web.backend.providers.ManagementUsersAuthenticationProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

@Configuration
@ConfigurationProperties( prefix = "life.basic-auth")
@ConditionalOnProperty(value = "life.basic-auth.enabled", havingValue = "true", matchIfMissing = true)
public class ManagementUsersConfig {

    private List<UserInfo> userInfos;

    public ManagementUsersConfig() {
        this.userInfos = Collections.emptyList();
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    @Bean
    public ManagementUsersAuthenticationProvider managementUsersAuthenticationProvider(ManagementUsersConfig config) {
        return new ManagementUsersAuthenticationProvider(config);
    }
}