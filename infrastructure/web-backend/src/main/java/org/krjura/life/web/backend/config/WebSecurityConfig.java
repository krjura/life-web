package org.krjura.life.web.backend.config;

import org.krjura.life.web.backend.enums.Privileges;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/actuator/health").permitAll()
                    .antMatchers("/actuator").hasAnyAuthority(Privileges.MONITORING.name())
                    .antMatchers("/actuator/**").hasAnyAuthority(Privileges.MONITORING.name())
                    .antMatchers("/**").permitAll();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);

        http.csrf().disable();

        http
                .httpBasic()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint());

        http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}