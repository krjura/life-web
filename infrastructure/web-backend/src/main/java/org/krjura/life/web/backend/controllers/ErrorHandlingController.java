package org.krjura.life.web.backend.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class ErrorHandlingController implements ErrorController {

    private static final String PATH = "/error";

    @Override
    public String getErrorPath() {
        return null;
    }

    @RequestMapping(PATH)
    public ResponseEntity<FileSystemResource> error() {
        if(Files.exists(Paths.get("resources/index.html"))) {
            return ResponseEntity.ok(new FileSystemResource("resources/index.html"));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
