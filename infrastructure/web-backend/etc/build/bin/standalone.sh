#!/bin/bash

APPLICATION_HOME=$(cd `dirname $0` && cd .. && pwd)
cd "$APPLICATION_HOME"

JAVACMD=java
JAVA_MAIN_CLASS=org.krjura.life.web.backend.Application

# set default if not present
if [ -z "$DEFAULT_JVM_MEM_OPTS" ]
then
    DEFAULT_JVM_MEM_OPTS="-Xms64M -Xmx128M"
fi

# build classpath
CLASSPATH=""
FIRST=0

for jar in lib/*.jar
do
  if [ "$FIRST" -eq "0" ]
  then
    CLASSPATH=$jar
    FIRST=1
  else
    CLASSPATH=$CLASSPATH:$jar
  fi
done

# build command
COMMAND="$JAVACMD $DEFAULT_JVM_OPTS $WEB_BACKEND $DEFAULT_JVM_MEM_OPTS -classpath $CLASSPATH $JAVA_MAIN_CLASS"

# execute
$COMMAND