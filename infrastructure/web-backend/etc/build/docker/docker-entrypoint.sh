#!/usr/bin/env bash

# Since bash does not propagate signals properly we need to do this so docker can stop the container properly
_term() {
  echo "Caught SIGTERM signal in docker-entrypoint.sh!"
  MAIN_PID=$(cat $MAIN_PID_FILE)
  kill $MAIN_PID 2>/dev/null
  # must wait for the script to exit
  wait $RUN_SCRIPT_PID
}

trap _term SIGTERM

# change user privileges
mkdir -p $APPLICATION_HOME/logs
mkdir -p $APPLICATION_HOME/tmp
mkdir -p $APPLICATION_HOME/running

chown -R $APPLICATION_USER:$APPLICATION_GROUP $APPLICATION_HOME
chmod -R a-w $APPLICATION_HOME
chmod -R o-rwx $APPLICATION_HOME
chmod -R ug+w $APPLICATION_HOME/running $APPLICATION_HOME/logs $APPLICATION_HOME/tmp

#Start application
export MAIN_PID_FILE=$APPLICATION_HOME/tmp/application.pid
su $APPLICATION_USER bin/standalone.sh &
RUN_SCRIPT_PID=$!
wait $RUN_SCRIPT_PID