#!groovy

node {

  emailRecipients = 'krjura@gmail.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()
    throw err
  }
}

def build() {

  resolveProperties()

  stage('Initialize') {
    checkout scm
  }

  stage('Build Frontend') {
    withCredentials([
      usernamePassword(credentialsId: 'MAVEN2_KRJURA_ORG_CREDENTIALS', usernameVariable: 'REPOSITORY_USERNAME', passwordVariable: 'REPOSITORY_PASSWORD')
    ]) {
      withEnv([
        'GRADLE_USER_HOME=/opt/cache/gradle',
        'ENVIRONMENT=jenkins']) {

        withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {

          docker.image('docker.krjura.org/life/java-build-env:v10').inside("-v life-gradle-cache:${GRADLE_USER_HOME}") {
            sh './build-utils --module infrastructure/web-backend --branch ${BRANCH_NAME} --env $ENVIRONMENT --op installDist'
          }

          docker.image('docker.krjura.org/life/angular-build-env:life-v6').inside("-v npm-cache:/opt/node/npm-cache") {
            sh './build-utils --module web/frontend --file etc/jenkins/build-frontend.sh --op script'
          }

          IMAGE_TAG="docker.krjura.org/life/frontend:1.${BUILD_NUMBER}"
          buildImage = docker.build("${IMAGE_TAG}", "-f Dockerfile-frontend ${WORKSPACE}")
          buildImage.push();

          sh "docker rmi ${IMAGE_TAG}"

          // checkout version project
          dir ('versions') {
            git credentialsId: 'jenkins@development_support', url: 'git@bitbucket.org:krjura/life-versions.git'
          }

          // add new version to versions project
          sh './build-versions --op=add --directory=versions --module=frontend --branch=$BRANCH_NAME --version=' + BUILD_NUMBER
          // commit changes if any
          sh './build-versions --op=commit --directory=versions --branch=$BRANCH_NAME'
        }
      }
    }
  }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
    body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
      subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
      body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []

  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '20')))

  config.add(disableConcurrentBuilds())

  properties(config)
}
